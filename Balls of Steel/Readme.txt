Balls of Steel - Labor Games SS 2017
Simon Kumm: 187070
Jens Niedermeier: 185581



Resourcen:
Die meisten von uns erstellten Resourcen befinden sich im Resources Ordner.
Im Pfad "\Resources\BallsOfSteel\Scripts\VRScripts\" befinden sich unsere Scripts für die Steuerung des Flyers mit Hilfe der VIVE Tracked Controllers, als auch die Scripts für die VR Interaktion mit den UI Elementen.

Das Script, welches die Bewegung der Kamera in der VR Umgebung herausrechnet befindet sich wiederum in "\VRSampleScenes\Scripts\Flyer\FlyerMovementController.cs". Hier wird auch die Bewegung des Flyers behandelt.
