﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using VRStandardAssets.Flyer;

namespace BallsOfSteel {

    public class Freezer : NetworkBehaviour {

        public float missilespeed;
        public float maxtime;
        public float startdelay;
        public float freezetime;
        public Transform target;

        private GameObject hitted;

        // Use this for initialization
        void Start() {
            GetComponent<Rigidbody>().AddForce(transform.forward * 5000);
            Destroy(gameObject, maxtime);
        }
        public void setTarget(Transform target)
        {
            this.target = target;
        }
        // Update is called once per frame
        void Update() {
            if (startdelay > 0)
            {
                startdelay -= Time.deltaTime;
            } else
            {
                transform.LookAt(target);
                GetComponent<Rigidbody>().AddForce(transform.forward * missilespeed);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            //Debug.LogError("BAZINGA EPICFAIL");
            if (other.CompareTag("Player") & (startdelay <= 0))
            {
                hitted = other.gameObject;
                hitted.GetComponent<FlyerMovementController>().setFreezed(freezetime);
                //NetworkServer.objects.Remove(gameObject.GetComponent<NetworkInstanceId>());
                //Destroy(gameObject.GetComponent<Rigidbody>());
                //Destroy(gameObject.GetComponent<NetworkIdentity>());
                //Destroy(gameObject.GetComponent<NetworkTransform>());
                //transform.parent = other.gameObject.transform;
                //Destroy(gameObject, freezetime);
                startdelay = 500f;
                Debug.LogError("BAZINGA");
                NetworkServer.Destroy(gameObject);
            }
            else if (startdelay <= 0)
            {
                Debug.LogError("BAZINGA FAIL");
                NetworkServer.Destroy(gameObject);
            }
        }
    }
}