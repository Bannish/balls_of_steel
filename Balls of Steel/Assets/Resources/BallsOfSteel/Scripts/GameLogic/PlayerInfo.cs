﻿namespace BallsOfSteel
{
    public class PlayerInfo
    {
        public string PlayerName { get; private set; }

        public uint PlayerID { get; private set; }

        public int Score { get; set; }

        public PlayerInfo(uint playerID, string playerName, int score = 0)
        {
            PlayerID = playerID;
            PlayerName = playerName;
            Score = score;
        }

        public void ChangeScore(int newScore)
        {
            Score = newScore;
        }

        public override string ToString()
        {
            return "PlayerName: " + PlayerName + " PlayerID: " + PlayerID + " Score: " + Score;
        }
    }
}
