﻿using Prototype.NetworkLobby;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using VRStandardAssets.Flyer;

namespace BallsOfSteel
{

    public class GameLogic : NetworkBehaviour
    {

        public static GameLogic Instance { get; private set; }

        [SyncVar]
        public int BallNumber;

        public GameObject ballPrefab;


        public int maxBalls = 1;

        public int scoreMax = 10;

        [SyncVar]
        public bool gameEnded;

        private void Awake()
        {
            gameEnded = true;
            Instance = this;
        }

        public override void OnStartServer()
        {

        }

        // Use this for initialization
        void Start()
        {
            Invoke("StartGame", 10f);
        }

        // Update is called once per frame
        void Update()
        {
            if (BallNumber < maxBalls && !gameEnded)
            {
                Vector3 pos = new Vector3(Random.Range(-2500, 2500), transform.position.y - Random.Range(0, 100), Random.Range(-2500, 2500));
                GameObject ball = Instantiate(ballPrefab, pos, Quaternion.identity);
                NetworkServer.Spawn(ball);
                BallNumber += 1;
            }
        }

        public void BallDestroyed()
        {
            BallNumber -= 1;
        }

        private void StartGame()
        {
            gameEnded = false;
        }
    }
}
