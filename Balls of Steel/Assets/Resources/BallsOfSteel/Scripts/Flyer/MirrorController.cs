﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallsOfSteel
{

    public class MirrorController : MonoBehaviour
    {

        public Camera mirrorLeft;
        public Camera mirrorRight;

        public RawImage rtLeft;
        public RawImage rtRight;

        public bool MirrorEnabled { get; set; }

        // Use this for initialization
        void Start()
        {
            MirrorEnabled = false;
            //mirrorLeft.enabled = false;
            //mirrorRight.enabled = false;
            rtLeft.enabled = false;
            rtRight.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (MirrorEnabled)
            {
                //mirrorLeft.enabled = true;
                //mirrorRight.enabled = true;
                rtLeft.enabled = true;
                rtRight.enabled = true;
            }
            else
            {
                //mirrorLeft.enabled = false;
                //mirrorRight.enabled = false;
                rtLeft.enabled = false;
                rtRight.enabled = false;
            }
        }

    }
}
