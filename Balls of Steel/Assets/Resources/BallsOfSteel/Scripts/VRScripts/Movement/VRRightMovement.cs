﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Flyer;

namespace BallsOfSteel
{

    public class VRRightMovement : MonoBehaviour
    {
        #region Button Name Rewrites
        private openVR.VR.EVRButtonId gripButton = openVR.VR.EVRButtonId.k_EButton_Grip;
        private openVR.VR.EVRButtonId triggerButton = openVR.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
        private openVR.VR.EVRButtonId touchpad = openVR.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
        private openVR.VR.EVRButtonId appMenuButton = openVR.VR.EVRButtonId.k_EButton_ApplicationMenu;
        private openVR.VR.EVRButtonId touchpadAxis = openVR.VR.EVRButtonId.k_EButton_Axis0;
        private openVR.VR.EVRButtonId triggerAxis = openVR.VR.EVRButtonId.k_EButton_Axis1;
        #endregion

        #region Controller Setup
        private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
        private SteamVR_TrackedObject trackedObj;
        #endregion

        #region Private Fields
        private FlyerMovementController movementscript;
        private MirrorController mirrorscript;
        #endregion

        // Use this for initialization
        void Start()
        {
            trackedObj = GetComponent<SteamVR_TrackedObject>();
            movementscript = GetComponentInParent<FlyerMovementController>();
            mirrorscript = GetComponentInParent<MirrorController>();

        }

        // Update is called once per frame
        void Update()
        {
            if (Controller == null)
            {
                Debug.Log("Controller not initialized");
                return;
            }
            if (Controller.GetPress(touchpadAxis))
            {
                if (Controller.GetAxis(touchpadAxis).y < -0.45f)
                {
                    movementscript.accback = true;
                }
                else
                {
                    movementscript.accback = false;
                }
                if (Controller.GetAxis(touchpadAxis).y > 0.45f)
                {
                    movementscript.accforward = true;
                }
                else
                {
                    movementscript.accforward = false;
                }
            }
            else
            {
                movementscript.accback = false;
                movementscript.accforward = false;

            }

            movementscript.makeacc = Controller.GetPress(triggerButton);
            movementscript.accdirection = GetComponent<Transform>().forward;

            if (Controller.GetPressDown(gripButton))
            {
                mirrorscript.MirrorEnabled = !mirrorscript.MirrorEnabled;
            }
            if(Controller.GetPressDown(appMenuButton))
            {
                
            }
        }
    }
}
