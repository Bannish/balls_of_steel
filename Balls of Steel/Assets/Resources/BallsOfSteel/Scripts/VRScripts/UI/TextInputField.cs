﻿using openVR.VR;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BallsOfSteel
{
    public class TextInputField : MonoBehaviour, VRMenuButton
    {
        private InputField input;
        public bool keyboardActive = false;


        public void Awake()
        {
            SteamVR_Events.System(openVR.VR.EVREventType.VREvent_KeyboardClosed).Listen(OnKeyboardClosed);
            SteamVR_Events.System(openVR.VR.EVREventType.VREvent_KeyboardCharInput).Listen(OnKeyboardCharInput);
            SteamVR_Events.System(openVR.VR.EVREventType.VREvent_ShowKeyboard).Listen(OnKeyboardShow);
            input = GetComponentInParent<InputField>();
        }

        public void VRButtonPress()
        {
            keyboardActive = true;
            SteamVR.instance.overlay.ShowKeyboard(0, 0, "Keyboard", 256, input.text, false, 0);
            
        }

        void Update()
        {
            
        }


        private void OnKeyboardCharInput(VREvent_t ev)
        {
        }

        private void OnKeyboardClosed(VREvent_t ev)
        {
            if(keyboardActive)
            {
                StringBuilder inputText = new StringBuilder("", 256);
                SteamVR.instance.overlay.GetKeyboardText(inputText, 256);
                input.text = inputText.ToString();
                //input.onEndEdit.Invoke(inputText.ToString()); //Methode muss noch in VR getestet werden. Möglicherweise wichtig für netzwerk übertragung
                keyboardActive = false;
            }
        }

        private void OnKeyboardShow(VREvent_t ev)
        {

        }
    }
}
