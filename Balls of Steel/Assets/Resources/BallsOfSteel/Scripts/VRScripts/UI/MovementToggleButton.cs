﻿using UnityEngine;
using UnityEngine.UI;

namespace BallsOfSteel
{
    public class MovementToggleButton : MonoBehaviour, VRMenuButton
    {
        public void VRButtonPress()
        {
            Toggle value = GetComponentInParent<Toggle>();

            GetComponentInParent<Toggle>().onValueChanged.Invoke(!value.isOn);
        }
    }
}
