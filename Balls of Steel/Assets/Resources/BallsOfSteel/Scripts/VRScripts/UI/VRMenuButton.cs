﻿namespace BallsOfSteel
{
    /// <summary>
    /// Interface for Button Presses in VR Environments
    /// </summary>
    public interface VRMenuButton
    {

        /// <summary>
        /// Methode to be implemented by the Objects that will be a VR button
        /// </summary>
        void VRButtonPress();
    }
}
