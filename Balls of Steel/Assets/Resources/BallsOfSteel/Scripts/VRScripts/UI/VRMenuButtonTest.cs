﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallsOfSteel
{

    public class VRMenuButtonTest : MonoBehaviour, VRMenuButton
    {

        private Button button;

        void Awake()
        {
            button = GetComponentInParent<Button>();
        }

        public void VRButtonPress()
        {
            button.onClick.Invoke();
        }
    }
}
