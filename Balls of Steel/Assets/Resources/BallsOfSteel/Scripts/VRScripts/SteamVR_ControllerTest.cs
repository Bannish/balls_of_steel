﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BallsOfSteel
{

    /// <summary>
    /// Diese Klasse zeigt im Unity Editor, welcher der Knöpfe des HTC Vive
    /// Tracked Controller gedrückt bzw. berührt wurden. Außerdem werden die
    /// Koordinaten des Druckpunkted auf dem Trackpad sowie die Werte des
    /// Triggers angezeigt.
    /// 
    /// Von Jens Niedermeier & Simon Kumm
    /// Im Rahmen der Blockwoche Spieleentwicklung und Labor Games
    ///
    /// SEB 2017
    /// </summary>
    public class SteamVR_ControllerTest : MonoBehaviour
    {
        // Den einzelnen Knöpfen werden hier einfachere Namen zugeordnet
        #region Button Name Rewrites
        private openVR.VR.EVRButtonId gripButton = openVR.VR.EVRButtonId.k_EButton_Grip;
        private openVR.VR.EVRButtonId triggerButton = openVR.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
        private openVR.VR.EVRButtonId touchpad = openVR.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
        private openVR.VR.EVRButtonId appMenuButton = openVR.VR.EVRButtonId.k_EButton_ApplicationMenu;
        private openVR.VR.EVRButtonId touchpadAxis = openVR.VR.EVRButtonId.k_EButton_Axis0;
        private openVR.VR.EVRButtonId triggerAxis = openVR.VR.EVRButtonId.k_EButton_Axis1;
        #endregion

        // Dieser Code wird benötigt, um den Controller mit der richtigen Device ID anzusprechen.
        #region Controller Setup
        private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
        private SteamVR_TrackedObject trackedObj;
        #endregion

        #region Test Variables for Controller
        public bool gripButtonPressed;
        public bool triggerPressed;
        public bool touchpadTouched;
        public bool appMenuButtonPressed;
        public bool touchpadAxisPressed;
        public Vector2 touchpadAxisValues;
        public bool triggerAxisPressed;
        public Vector2 triggerAxisValues;

        public Vector3 velocity;
        public Vector3 angularVelocity;
        #endregion

        #region Allgemeine Tipps und Tricks für die SteamVR Controller

        #region Aufbau des GameObject

        // Will man nur auf die Inputs des Controller zurückgreifen, so reicht es, ein GameObject zu erstellen, welches
        // als ein Container Dient. Dieses GameObject bekommt das "SteamVR_ControllerManager" Script angehängt, mit welchem man
        // festlegen kann, welcher controller der Linke und welcher der Rechte ist. Dieser Container bekommt ein neues GameObject als Child,
        // welches man nun "Controller (left)" nennen kann. Diesem GameObject wird das "SteamVR_TrackedObject" Script angehängt.
        // WICHTIG!!! Im TrackedObject Script KEINEN Index zuweisen. Dieser wird von Unity automatisch vergeben, wenn der Controller erkannt wurde.
        // Der Controller ist nun bereit, um die Inputs der Knöpfe und das Tracking zu verwenden.

        #endregion

        #region Sichtbarkeit der Controller

        // Möchte man die HTC Vive Controller im Spiel sichtbar machen, so sind ein paar kleine Änderungen nötig.
        // Als erstes sollte man dem Controller GameObject ein weiteres GameObject als Child hinzufügen. Dieses kann
        // man zum beispiel "Model" nennen. Diesem GameObject wird das "SteamVR_RenderModel" Script angehängt.
        // Auch diesem Script sollte man keinen Index zuweisen. Wichtig ist, dass "Create Components" und "Update Dynamically"
        // bei diesem Script aktiviert werden.

        #endregion

        #region Fehlerbehebung!

        // Sollten die Controller trotzdem nicht Sichtbar sein, so kann es Helfen, der MainCamera das "SteamVR_UpdatePoses" Script anzuhängen.

        #endregion

        #endregion
        // Use this for initialization
        void Start()
        {
            trackedObj = GetComponent<SteamVR_TrackedObject>();
        }

        // Update is called once per frame
        void Update()
        {
            if (Controller == null)
            {
                Debug.Log("Controller not initialized");
                return;
            }

            gripButtonPressed = Controller.GetPress(gripButton);
            triggerPressed = Controller.GetPress(triggerButton);
            touchpadTouched = Controller.GetTouch(touchpad);
            appMenuButtonPressed = Controller.GetPress(appMenuButton);

            touchpadAxisPressed = Controller.GetPress(touchpadAxis);
            touchpadAxisValues = Controller.GetAxis(touchpadAxis);

            triggerAxisPressed = Controller.GetPress(triggerAxis);
            triggerAxisValues = Controller.GetAxis(triggerAxis);

            velocity = Controller.velocity;
            angularVelocity = Controller.angularVelocity;
        }
    }
}
