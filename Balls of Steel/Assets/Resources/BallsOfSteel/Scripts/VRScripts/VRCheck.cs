﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

namespace BallsOfSteel
{

    public class VRCheck : MonoBehaviour
    {

        private void Awake()
        {
            if (!VRSettings.isDeviceActive)
            {
                Debug.Log("No VR Device active!");
                VRSettings.enabled = false;
                SceneManager.LoadSceneAsync("MainMenu");
            }
            else
            {
                Debug.Log("VR Device Active");
            }
        }

        public void EnableVR()
        {
            Debug.Log("VR Enabled");
            VRSettings.enabled = true;
            SceneManager.LoadSceneAsync("MainMenu");
        }

        public void DisableVR()
        {
            Debug.Log("VR Disabled");
            VRSettings.enabled = false;
            SceneManager.LoadSceneAsync("MainMenu");
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
