﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.VR;

namespace BallsOfSteel
{
    public class SettingsController : MonoBehaviour
    {

        public Toggle movement;

        private void Awake()
        {
            movement.isOn = PlayerGameSettings.Instance.DirectionalFlying;
        }

        public void SaveSettings()
        {
            Debug.Log("Not Yet Implemented");
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void DirectionalMovement(bool value)
        {
            PlayerGameSettings.Instance.SetDirectionalFlying(value);
        }


        public void BackToMainMenu()
        {
            SceneManager.LoadSceneAsync("MainMenu");
        }
    }
}
