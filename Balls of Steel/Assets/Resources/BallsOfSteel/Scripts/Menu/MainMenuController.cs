﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.VR;

namespace BallsOfSteel
{
    public class MainMenuController : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        private void Awake()
        {
            Canvas canv = GetComponent<Canvas>();

            if (!VRSettings.enabled)
            {
                canv.renderMode = RenderMode.ScreenSpaceOverlay;
            }
        }

        public void StartPlaying()
        {
            SceneManager.LoadSceneAsync("MenuLobby");
        }

        public void EditSettings()
        {
            SceneManager.LoadSceneAsync("SettingsMenu");
        }

        public void ShowCredits()
        {
            Debug.Log("Simon Kumm, Jens Niedermeier. Projekt im Rahmen der Blockwoche und Labor Games");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}
