﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BallsOfSteel {

    public class MenuController : MonoBehaviour {

        #region Button Name Rewrites
        private openVR.VR.EVRButtonId gripButton = openVR.VR.EVRButtonId.k_EButton_Grip;
        private openVR.VR.EVRButtonId triggerButton = openVR.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
        private openVR.VR.EVRButtonId touchpad = openVR.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
        private openVR.VR.EVRButtonId appMenuButton = openVR.VR.EVRButtonId.k_EButton_ApplicationMenu;
        private openVR.VR.EVRButtonId touchpadAxis = openVR.VR.EVRButtonId.k_EButton_Axis0;
        private openVR.VR.EVRButtonId triggerAxis = openVR.VR.EVRButtonId.k_EButton_Axis1;
        #endregion

        #region Controller Setup
        private SteamVR_Controller.Device Controller { get { return SteamVR_Controller.Input((int)trackedObj.index); } }
        private SteamVR_TrackedObject trackedObj;
        #endregion

        #region Public fields for setup purposes
        public bool active = true;
        public Color color;
        public float thickness = 0.002f;
        public GameObject holder;
        public GameObject pointer;
        public Collider rayHitCollider = null;
        #endregion

        private VRMenuButton reference = null;
        bool addRigidBody = false;
        bool isActive = false;

        // Use this for initialization
        void Start() {
            trackedObj = GetComponent<SteamVR_TrackedObject>();

            holder = new GameObject();
            holder.transform.parent = this.transform;
            holder.transform.localPosition = Vector3.zero;
            holder.transform.localRotation = Quaternion.identity;

            pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
            pointer.transform.parent = holder.transform;
            pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
            pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
            pointer.transform.localRotation = Quaternion.identity;
            BoxCollider collider = pointer.GetComponent<BoxCollider>();
            if (addRigidBody)
            {
                if (collider)
                {
                    collider.isTrigger = true;
                }
                Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
            }
            else
            {
                if (collider)
                {
                    Object.Destroy(collider);
                }
            }
            Material newMaterial = new Material(Shader.Find("Unlit/Color"));
            newMaterial.SetColor("_Color", color);
            pointer.GetComponent<MeshRenderer>().material = newMaterial;
        }

        // Update is called once per frame
        void Update() {
            if (!isActive)
            {
                isActive = true;
                this.transform.GetChild(0).gameObject.SetActive(true);
            }

            Ray raycast = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            bool bHit = Physics.Raycast(raycast, out hit);

            if(bHit)
            {
                rayHitCollider = hit.collider;
                reference = rayHitCollider.GetComponent<VRMenuButton>();
            }
            if(!bHit)
            {
                reference = null;
                rayHitCollider = null;
            }

            if(Controller.GetPressDown(triggerButton) && bHit)
            {
                if(reference != null)
                {
                    reference.VRButtonPress();
                }
                else
                {
                    Debug.Log("Null Ref");
                }
            }
        }
    }
}
