﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class PlayerGameSettings : MonoBehaviour {

    public static PlayerGameSettings Instance { get; private set; }
    public bool DirectionalFlying = false;
    public string playername { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        if(!VRSettings.isDeviceActive)
        {
            DirectionalFlying = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDirectionalFlying(bool mode = false)
    {
        DirectionalFlying = mode;
    }
}
