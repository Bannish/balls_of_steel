﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace BallsOfSteel
{
    public class Spawning : NetworkBehaviour
    {

        /// <summary>
        /// Ball prefab spawned after at Game Start and when a new Ball is Spawned.
        /// </summary>
        public GameObject ballPrefab;

        /// <summary>
        /// Number of Balls spawned at Game Start.
        /// </summary>
        public int ballMax;

        // Use this for initialization
        void Start()
        {
            for (int i = 0; i < ballMax; ++i)
            {
                RpcSpawnBall();
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        /// <summary>
        /// Spawns a new Ball, executed on Server!
        /// </summary>
        [ClientRpc]
        public void RpcSpawnBall()
        {
            if (!isServer)
            {
                return;
            }

            Vector3 pos = new Vector3(Random.Range(-2500, 2500), transform.position.y - Random.Range(0, 100), Random.Range(-2500, 2500));
            GameObject ball = Instantiate(ballPrefab, pos, Quaternion.identity);
            NetworkServer.Spawn(ball);
        }
    }
}
