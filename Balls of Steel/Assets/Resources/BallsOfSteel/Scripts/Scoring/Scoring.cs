﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using VRStandardAssets.Flyer;

namespace BallsOfSteel
{

    public class Scoring : NetworkBehaviour
    {

        public GameObject flyer;
        private Transform anchor;
        private Material[] selectedMaterials;

        // Use this for initialization
        void Start()
        {
            selectedMaterials = Resources.LoadAll<Material>("BallsOfSteel/Materials/ScoringBall/");
            gameObject.GetComponent<Renderer>().material = selectedMaterials[Random.Range(0, selectedMaterials.Length)];
        }


        // Update is called once per frame
        void Update()
        {

        }

        void FixedUpdate()
        {
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                anchor = other.GetComponent<Transform>().Find("BallAnchor");
                if (anchor != null)
                {
                    flyer = other.gameObject;
                    transform.parent = anchor;
                    /**
                    transform.GetComponent<Rigidbody>().isKinematic = true;
                    transform.position = anchor.position;
                    transform.localScale = new Vector3(8, 8, 8);
                    */

                    flyer.GetComponent<FlyerMovementController>().PlayerScored(1);

                    if (isServer)
                    {
                        GameLogic.Instance.BallDestroyed();
                    }

                    Destroy(gameObject);
                }
            }
        }
    }
}
