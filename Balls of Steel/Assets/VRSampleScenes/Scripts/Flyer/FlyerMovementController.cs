﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.VR;
using UnityEngine.Networking;
using BallsOfSteel;
using UnityEngine.SceneManagement;

namespace VRStandardAssets.Flyer
{
    // This class handles the movement of the flyer ship.
    public class FlyerMovementController : NetworkBehaviour
    {
        [SerializeField]
        private float m_DistanceFromCamera = 75f;  // The distance from the camera the ship aims to be.
        [SerializeField]
        private float m_Speed = 100f;              // The speed the ship moves forward.
        [SerializeField]
        private float m_Damping = 0.5f;            // The amount of damping applied to the movement of the ship.
        [SerializeField]
        private Transform m_Flyer;                 // Reference to the transform of the flyer.
        [SerializeField]
        private Transform m_TargetMarker;          // The transform the flyer is moving towards.
        [SerializeField]
        private Transform m_Camera;                // Reference to the camera's transform.
        [SerializeField]
        private Transform m_CameraContainer;       // Reference to the transform containing the camera.
        [SerializeField]
        private Text m_CurrentScore;               // Reference to the Text component that will display the user's score.

        public Transform rocket;
        public GameObject Freezer;
        public float mousesense;
        public float rollingspeed;
        public GameObject Flyer;
        public float maxcam;
        public float mincam;
        public GameObject cam;
        public bool accforward;
        public bool accback;
        public bool accleft;
        public bool accright;
        public bool accup;
        public bool accdown;
        // new VR controll
        public bool makeacc;
        public Vector3 accdirection;

        public Text NotificationBar;
        public Text points;

        [SyncVar]
        private string PlayerName;
        [SyncVar]
        private uint PlayerID;
        [SyncVar]
        public int PlayerScore;

        private uint lockedTarget;
        private bool allowtofire = true;
        private bool freezed;
        private bool m_IsGameRunning;                               // Whether the game is running.
        private Vector3 m_FlyerStartPos;                            // These positions and rotations are stored at Start so the flyer can be reset each game.
        private Quaternion m_FlyerStartRot;
        private Vector3 m_TargetMarkerStartPos;
        private Quaternion m_TargetMarkerStartRot;
        private Vector3 m_CameraContainerStartPos;

        private const float k_ExpDampingCoef = -20f;                // The coefficient used to damp the movement of the flyer.
        private const float k_BankingCoef = 3f;                     // How much the ship banks when it moves.

        private void OnAwake()
        {
            //GetComponentInChildren<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            //GetComponentInChildren<Rigidbody>().enable = false;
        }

        private void Start()
        {
            //Cursor.lockState = CursorLockMode.Locked;
            // Store all the starting positions and rotations.
            m_FlyerStartPos = m_Flyer.position;
            m_FlyerStartRot = m_Flyer.rotation;
            m_TargetMarkerStartPos = m_TargetMarker.position;
            m_TargetMarkerStartRot = m_TargetMarker.rotation;
            m_CameraContainerStartPos = m_CameraContainer.position;
            //m_Camera.position = new Vector3(m_Flyer.position.x, m_Flyer.position.y, m_Flyer.position.z - 50);

            if (isLocalPlayer)
            {
                SetPlayerOptions();
                return;
            }
            cam.SetActive(false);
            this.enabled = false;
        }

        private void SetPlayerOptions()
        {
            Debug.Log("IsLocalPlayer");
            PlayerName = PlayerGameSettings.Instance.playername;
            PlayerID = netId.Value;
            PlayerScore = 0;
            Debug.Log("Player: " + PlayerName + " ID: " + PlayerID + " Score: " + PlayerScore);
            return;
        }

        // methode um gefreezed zu werden!
        public void setFreezed(float freezetime)
        {
            Debug.Log(netId.Value + "wurde gefreezed!");
            freezed = true;
            Invoke("defreeze", freezetime);
        }

        private void defreeze()
        {
            freezed = false;
        }
        
        public void StartGame()
        {
            // The game is now running.
            m_IsGameRunning = true;
            // Start the flyer moving.
            StartCoroutine(MoveFlyer());
        }

        public void StopGame()
        {
            // The game is no longer running.
            m_IsGameRunning = false;

            // Reset all the positions and rotations that were store.
            m_Flyer.position = m_FlyerStartPos;
            m_Flyer.rotation = m_FlyerStartRot;
            m_TargetMarker.position = m_TargetMarkerStartPos;
            m_TargetMarker.rotation = m_TargetMarkerStartRot;
            m_CameraContainer.position = m_CameraContainerStartPos;
        }

        void Update()
        {

            if (!isLocalPlayer)
            {
                return;
            }

            if (VRSettings.enabled)
            {

                Quaternion headRotation = InputTracking.GetLocalRotation(VRNode.Head);

                // do rotation
                Vector3 oldvelodirection = m_Flyer.GetComponent<Rigidbody>().velocity;
                m_Flyer.rotation = headRotation;
                Quaternion cameranull = new Quaternion(headRotation.x * -1.0f, headRotation.y * -1.0f, headRotation.z * -1.0f, headRotation.w);
                m_CameraContainer.localRotation = cameranull;

                //m_Flyer.GetComponent<Rigidbody>().velocity =
                //    headRotation *
                //    m_Flyer.transform.forward.normalized *
                //    m_Flyer.GetComponent<Rigidbody>().velocity.magnitude;//   m_Flyer.transform.forward * velo * direction;
                //Vector3 drift = (oldvelodirection - m_Flyer.GetComponent<Rigidbody>().velocity) * 0.2f;
                //m_Flyer.GetComponent<Rigidbody>().velocity = (m_Flyer.GetComponent<Rigidbody>().velocity + drift).normalized * m_Flyer.GetComponent<Rigidbody>().velocity.magnitude;
                // "debug" ausgabe auf flyer HUD
                m_CurrentScore.text = "" + m_Flyer.GetComponent<Rigidbody>().velocity.magnitude;

                if (PlayerGameSettings.Instance.DirectionalFlying)
                {
                    accdirection = (cameranull * accdirection).normalized;
                    if (makeacc)
                    {
                        GetComponent<Rigidbody>().AddForce(accdirection * m_Speed);
                    }
                    m_CurrentScore.text = "" + m_Flyer.transform.forward;
                }
                else
                {
                    // movements
                    Vector3 accelaration = Vector3.zero;
                    //forward -- backwards ( speed? )
                    if (accforward)
                    {
                        accelaration = (accelaration + m_Flyer.transform.forward).normalized;
                    }
                    if (accback)
                    {
                        accelaration = (accelaration - m_Flyer.transform.forward.normalized).normalized;
                    }
                    // left -- right ( strafe )
                    if (accleft)
                    {
                        accelaration = (accelaration - m_Flyer.transform.right.normalized).normalized;
                    }
                    if (accright)
                    {
                        accelaration = (accelaration + m_Flyer.transform.right.normalized).normalized;
                    }
                    // up -- down
                    if (accdown)
                    {
                        accelaration = (accelaration - m_Flyer.transform.up.normalized).normalized;
                    }
                    if (accup)
                    {
                        accelaration = (accelaration + m_Flyer.transform.up.normalized).normalized;
                    }
                    if (!freezed)
                    {
                        GetComponent<Rigidbody>().AddForce(accelaration * m_Speed);
                    }
                }
            }
            else
            {
                float wheel = Input.GetAxis("Mouse ScrollWheel") * mousesense * 4;
                m_Camera.Translate(new Vector3(0, 0, wheel));

                // get rotation from Mouseinputs
                float movehorizontal = Input.GetAxis("Mouse X");
                float movevertikal = -Input.GetAxis("Mouse Y");
                // get rotation rolling from keys q and e
                float rolling = 0;
                if (Input.GetKey("q") || Input.GetKey("e"))
                {
                    if (Input.GetKey("q") && Input.GetKey("e"))
                    {
                        rolling = 0;
                    }
                    else if (Input.GetKey("q"))
                    {
                        rolling = rollingspeed;
                    }
                    else if (Input.GetKey("e"))
                    {
                        rolling = -rollingspeed;
                    }
                }
                // do rotation
                Vector3 oldvelodirection = Flyer.GetComponent<Rigidbody>().velocity;
                Quaternion rotate = Quaternion.FromToRotation(m_Flyer.transform.forward.normalized, Flyer.GetComponent<Rigidbody>().velocity.normalized);
                m_Flyer.Rotate(movevertikal * mousesense, movehorizontal * mousesense, rolling);
                Flyer.GetComponent<Rigidbody>().velocity =
                     rotate *
                    m_Flyer.transform.forward.normalized *
                    Flyer.GetComponent<Rigidbody>().velocity.magnitude;//   m_Flyer.transform.forward * velo * direction;*/
                Vector3 drift = (oldvelodirection - Flyer.GetComponent<Rigidbody>().velocity) * 0.5f;
                Flyer.GetComponent<Rigidbody>().velocity = (Flyer.GetComponent<Rigidbody>().velocity + drift).normalized * Flyer.GetComponent<Rigidbody>().velocity.magnitude;
                m_CurrentScore.text = "" + Flyer.GetComponent<Rigidbody>().velocity.magnitude;

                // movements
                Vector3 accelaration = Vector3.zero;
                //forward -- backwards ( speed? )
                if (Input.GetKey("w") || Input.GetKey("s"))
                {
                    if (Input.GetKey("w") && Input.GetKey("s")) { }
                    if (Input.GetKey("w"))
                    {
                        accelaration = (accelaration + m_Flyer.transform.forward.normalized).normalized;
                    }
                    if (Input.GetKey("s"))
                    {
                        accelaration = (accelaration - m_Flyer.transform.forward.normalized).normalized;
                    }
                }
                //m_CurrentScore.text = "" + Input.GetAxis("Vertical");


                // left -- right ( strafe )
                if (Input.GetKey("a") || Input.GetKey("d"))
                {
                    if (Input.GetKey("a") && Input.GetKey("d")) { }
                    else if (Input.GetKey("a")) // links
                    {
                        accelaration = (accelaration - m_Flyer.transform.right.normalized).normalized;
                    }
                    else if (Input.GetKey("d")) // rechts
                    {
                        accelaration = (accelaration + m_Flyer.transform.right.normalized).normalized;
                    }
                }

                // up -- down 

                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.Space))
                {
                    if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Space)) { }
                    else if (Input.GetKey(KeyCode.LeftShift))
                    {
                        accelaration = (accelaration - m_Flyer.transform.up.normalized).normalized;
                    }
                    else if (Input.GetKey(KeyCode.Space))
                    {
                        accelaration = (accelaration + m_Flyer.transform.up.normalized).normalized;
                    }
                }
                if (!freezed)
                {
                    Flyer.GetComponent<Rigidbody>().AddForce(accelaration * m_Speed);// * Time.deltaTime);
                }
                if (Input.GetKeyDown(KeyCode.Tab))
                {
                    GetComponent<MirrorController>().MirrorEnabled = !GetComponent<MirrorController>().MirrorEnabled;
                }
            }
            if (Input.GetButton("Fire1") & allowtofire)
            {
                // nur um alle spieler zu bekommen!!
                GameObject[] allplayers = GameObject.FindGameObjectsWithTag("Player");
                foreach (GameObject p in allplayers)
                {
                    //hier drin checken ob im sichtfeld
                    Vector3 screenPoint = cam.GetComponent<Camera>().WorldToViewportPoint(p.transform.position);
                    bool onScreen = screenPoint.z > 0 && screenPoint.x > 0 && screenPoint.x < 1 && screenPoint.y > 0 && screenPoint.y < 1;
                    if (onScreen & p.transform != gameObject.transform)
                    {
                        // kleinsten abstand als "locked" target setzten
                        if (lockedTarget != null)
                        {
                            float actdistance = (transform.position - lockedTarget.position).magnitude;
                            if ((transform.position - p.transform.position).magnitude < actdistance)
                            {
                                lockedTarget = p.Playerinfo.netID;
                            }
                        }
                        lockedTarget = p.transform;
                    }
                }
                if (lockedTarget != null)
                {
                    GameObject shoot = Instantiate(Freezer, rocket.position, rocket.rotation);
                    //rocket.gameObject.SetActive(false);
                    rocket.GetComponent<Renderer>().enabled = false;
                    allowtofire = false;
                    CmdShoot(shoot,lockedTarget);
                    Invoke("reshowrocket", 10f);
                }
            }
        }
        [Command]
        private void CmdShoot(GameObject shoot,Transform Target)
        {
            NetworkServer.Spawn(shoot);
            shoot.GetComponent<Freezer>().setTarget(Target);
        }

        private void reshowrocket()
        {
            allowtofire = true;
            rocket.GetComponent<Renderer>().enabled = true;
        }

        private IEnumerator MoveFlyer()
        {
            while (m_IsGameRunning)
            {
                /*// Set the target marker position to a point forward of the camera multiplied by the distance from the camera.
				Quaternion headRotation = InputTracking.GetLocalRotation (VRNode.Head);
                m_TargetMarker.position = m_Camera.position + (headRotation * Vector3.forward) * m_DistanceFromCamera;

                // Move the camera container forward.
                m_CameraContainer.Translate (Vector3.forward * Time.deltaTime * m_Speed);

                // Move the flyer towards the target marker.
                m_Flyer.position = Vector3.Lerp(m_Flyer.position, m_TargetMarker.position,
                    m_Damping * (1f - Mathf.Exp (k_ExpDampingCoef * Time.deltaTime)));

                // Calculate the vector from the target marker to the flyer.
                Vector3 dist = m_Flyer.position - m_TargetMarker.position;

                // Base the target markers pitch (x rotation) on the distance in the y axis and it's roll (z rotation) on the distance in the x axis.
                m_TargetMarker.eulerAngles = new Vector3 (dist.y, 0f, dist.x) * k_BankingCoef;

                // Make the flyer bank towards the marker.
                m_Flyer.rotation = Quaternion.Lerp(m_Flyer.rotation, m_TargetMarker.rotation,
                    m_Damping * (1f - Mathf.Exp (k_ExpDampingCoef * Time.deltaTime)));

                // Update the score text.
                //m_CurrentScore.text = "Score: " + SessionData.Score;
                */



                // Wait until next frame.

                yield return null;
            }
        }

        public void PlayerScored(int addedScore = 1)
        {
            PlayerScore += addedScore;
            Debug.Log("Player: " + PlayerName + " ID: " + PlayerID + " Score: " + PlayerScore);
            points.text = "SCORE: " + PlayerScore;

            if (isLocalPlayer)
            {
                if (PlayerScore >= GameLogic.Instance.scoreMax)
                {
                    CmdEndGame(PlayerName);
                }

                else
                {
                    CmdNotifyPlayers(PlayerName, "SCORED", 2f);
                }
            }
        }

        #region Notification System

        [ClientRpc]
        private void RpcNotifyPlayers(string message, float messageTime)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject item in players)
            {
                if (item.GetComponent<NetworkIdentity>().isLocalPlayer)
                {
                    item.GetComponent<FlyerMovementController>().SetNotificationText(message);
                    StartCoroutine(ClearNotification(item, messageTime));
                }
            }
        }


        IEnumerator ClearNotification(GameObject item, float messageTime)
        {
            yield return new WaitForSeconds(messageTime);

            item.GetComponent<FlyerMovementController>().SetNotificationText("");
        }

        [Command]
        public void CmdNotifyPlayers(string playerName, string message, float messageTime)
        {
            if (isServer)
            {
                RpcNotifyPlayers(playerName + " " + message, messageTime);
            }
        }

        public void AfterGameSceneSwitch()
        {
            StartCoroutine(SceneSwitch());
        }

        IEnumerator SceneSwitch()
        {
            yield return new WaitForSeconds(15f);
            Destroy(NetworkManager.singleton.gameObject);
            SceneManager.LoadSceneAsync("MainMenu");
        }

        public void SetNotificationText(string text)
        {
            NotificationBar.text = text;
        }

        [Command]
        public void CmdEndGame(string playerName)
        {
            if (isServer)
            {
                RpcEndGame(playerName);
            }
        }

        [ClientRpc]
        public void RpcEndGame(string playerName)
        {
            GameLogic.Instance.gameEnded = true;
            GameObject[] balls = GameObject.FindGameObjectsWithTag("Ball");
            foreach (var item in balls)
            {
                Destroy(item);
            }
            Debug.Log(playerName + " Won!");

            CmdNotifyPlayers(playerName, " won the Balls of Steel", 10f);
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject item in players)
            {
                if (item.GetComponent<NetworkIdentity>().isLocalPlayer)
                {
                    item.GetComponent<FlyerMovementController>().AfterGameSceneSwitch();
                }
            }
        }
    }
    #endregion
}
